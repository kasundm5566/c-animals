﻿using Animals.AnimalCapabilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals.AllAnimals {
    class Dog : Animal, IWalkable, IRunnable, ISoundMakable {
        public Dog(string name) : base(name) {
        }

        public void MakeSound() {
            Console.WriteLine("I am barking.");
        }

        public void Run() {
            Console.WriteLine("I am runnning.");
        }

        public void Walk() {
            Console.WriteLine("I am walking.");
        }
    }
}
