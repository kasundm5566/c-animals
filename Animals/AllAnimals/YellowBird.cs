﻿using Animals.AnimalCapabilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals.AllAnimals {
    class YellowBird : Animal, IFlyable, ISoundMakable {
        public YellowBird(string name) : base(name) {
        }

        public void Fly() {
            Console.WriteLine("I am flying.");
        }

        public void MakeSound() {
            Console.WriteLine("I am chirping.");
        }
    }
}
