﻿using Animals.AllAnimals;
using Animals.AnimalCapabilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals {
    class Program {
        static void Main(string[] args) {

            YellowBird yellowBird = new YellowBird("Yellow Bird 01");
            Dog myDog = new Dog("Dog 01");

            List<Animal> animalsList = new List<Animal>();
            animalsList.Add(yellowBird);
            animalsList.Add(myDog);

            for(int i = 0; i < animalsList.Count; i++) {
                if(animalsList.ElementAt(i) is IFlyable) {
                    animalsList.ElementAt(i).Fly();
                }
            }

            Console.ReadLine();
        }
    }
}
